﻿using HologramSpriteManager;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;

namespace StellarRats
{
    /// <summary>
    /// This is the main type for your game
    /// </summary>
    public class StellarRatsGame : Game
    {
        GraphicsDeviceManager graphics;
        public static Texture2D tx2hackBar;
        public static Texture2D tx2hackBarEnd;
        public static Vector2 hackbarpositon;
        public static Rectangle hackbarRectangle;
        public static Rectangle hackbarRectangleEnd;


        public StellarRatsGame()
        {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";

            graphics.PreferredBackBufferHeight = 1080;
            graphics.PreferredBackBufferWidth = 1920;

        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            SpriteManager.Initialise(new SpriteBatch(GraphicsDevice), Content, graphics);

            GameStatic.camera = new Camera(GraphicsDevice.Viewport);
            // TODO: Add your initialization logic here
            //TheCharacter = new PlayerCharacter();
            GameStatic.Initialise();
            base.Initialize();
        }


        protected override void LoadContent()
        {

            GameStatic.LoadLevel(1);

            GameStatic.tx2Transporter = Content.Load<Texture2D>("Sprites/Transporters/Teleport");
            GameStatic.tx2Health = Content.Load<Texture2D>("Sprites/Usable/Health");
            GameStatic.tx2Computer = Content.Load<Texture2D>("Sprites/Computer/Computer");
            GameStatic.tx2Doors = Content.Load<Texture2D>("Sprites/Doors/DoorTile0");
            tx2hackBar = Content.Load<Texture2D>("Sprites/Computer/HackBar");
            tx2hackBarEnd = Content.Load<Texture2D>("Sprites/Computer/HackBarEnd");
            hackbarpositon = new Vector2(100, 100);
            hackbarRectangle = new Rectangle(0,0,tx2hackBar.Width,tx2hackBar.Height);
            hackbarRectangleEnd = new Rectangle(0, 0, tx2hackBarEnd.Width - 2, tx2hackBarEnd.Height);

            for (int i = 0; i <2; i++)
            {
                Texture2D currentTexture = Content.Load<Texture2D>("Sprites/Ground/groundTile" + i);
                GameStatic.tx2Ground.Add(currentTexture);

            }
           
            for (int i = 0; i <4; i++)
            {
                Texture2D currentTexture = Content.Load<Texture2D>("Sprites/EnemyBase/EnemyBaseTile" + i);
                GameStatic.tx2EnemyBase.Add(currentTexture);
            }

            for (int i = 0; i < 4; i++)
            {
                Texture2D currentTexture = Content.Load<Texture2D>("Sprites/EnemyBase/DamageBaseTile" + i);
                GameStatic.tx2DamagedEnemyBase.Add(currentTexture);


            }
            for (int i = 0; i < 4; i++)
            {
                Texture2D currentTexture = Content.Load<Texture2D>("Sprites/EnemyBase/halfDamageBaseTile" + i);
                GameStatic.tx2HalfDamagedEnemyBase.Add(currentTexture);
            }
            for (int i = 0; i < 75; i++)
            {
                Texture2D currentTexture = Content.Load<Texture2D>("Sprites/Wall/" + i);
                GameStatic.tx2Wall.Add(currentTexture);

            }
            GameStatic.tx2PlayerIndicator  = Content.Load<Texture2D>("Sprites/Players/PColor");
        }


        protected override void UnloadContent()
        {
            // TODO: Unload any non ContentManager content here
        }

        protected override void Update(GameTime gameTime)
        {
            SpriteManager.Update(gameTime);
            // TODO: Add your update logic here
            GameStatic.Update();
            GameStatic.camera.Update(gameTime);

            Debug.Update();

            base.Update(gameTime);

        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>

        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.Black);
            // TODO: Add your drawing code here
            SpriteManager.spriteBatch.Begin(SpriteSortMode.Deferred, BlendState.AlphaBlend, null, null, null, null, GameStatic.camera.transform);
            GameStatic.Draw();
            SpriteManager.spriteBatch.End();


            SpriteManager.spriteBatch.Begin();
            GameStatic.DrawUI();
            SpriteManager.spriteBatch.End();


            base.Draw(gameTime);
        }
    }
}
