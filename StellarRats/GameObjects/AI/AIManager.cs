﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StellarRats
{
    public class AIManager
    {
        Dictionary<int, MapEnemyBase> lstEnemyBases = new Dictionary<int, MapEnemyBase>();

        public List<PreventerEnemy> currentPreventer = new List<PreventerEnemy>();



        public double distanceCompBase;

        public AIManager(List<MapEnemyBase> enemyBases)
        {
            foreach (var enemyBase in enemyBases)
            {
                
                lstEnemyBases.Add(enemyBase.ID, enemyBase);
            }
            //lstEnemyBases = enemyBases.ToDictionary(x => x.ID, x => x);
        }

        public void Update()
        {          //checkPoint();
            CheckPoints();
            FillEnemies();
        }

        private void FillEnemies()
        {
            foreach (Enemy enem in GameStatic.lstEnemy)
            {
                if (enem is PreventerEnemy)
                {
                    currentPreventer.Add((PreventerEnemy)enem);

                }
            }
        }
        public void CheckPoints()
        { //lambda :):)
            foreach (var comp in GameStatic.CurrentLevel.Computer)
            {
                if (!comp.enemOnComp && !comp.enemToComp)
                {    comp.enemToComp = true;
                    foreach (var baseCurrent in lstEnemyBases.Values)
                    {
                    
                       //guard ve canlı olan enemy varsa
                      //  bool enemyCheck = GameStatic.lstEnemy.Any(y => y._enemyTask == Enemy.EnemyTask.Guard && y.DestroyMe == false);
                        // bool compCheck = GameStatic.CurrentLevel.Computer.Any(x => x.enemOnComp == false);
                        // distanceCompBase = Math.Sqrt((Math.Pow((baseCurrent.X - comp.X), 2)) + (Math.Pow((baseCurrent.Y - comp.Y), 2)));   
           
                            if (baseCurrent.isAlive )
                            {     //send guard command  

                                baseCurrent.sendGuard = true;
                                baseCurrent.sendTargetPosition = new Vector2(comp.X, comp.Y);
                                

                                //Guard can be a function or an enum which will query by preventer enemy
                            }

                                
                                //son:::  if (baseCurrent.isAlive && !baseCurrent.sendGuard)
                            else
                            {
                                return;
                                //en yakın düşmanı bul
                                //en yakın düşmana koruma görevi ver

                            }
                       
                    }
                }

            }

        }
    }
}