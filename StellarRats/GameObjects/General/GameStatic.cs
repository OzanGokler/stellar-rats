﻿using HologramSpriteManager;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

﻿namespace StellarRats
 {
    class GameStatic
    {//AI Manager
        public static AIManager AIManager;
         //Database
         public static GameDatabase Database;

         //font
         public static FontRenderer MainFont;
         //camera
         public static Camera camera;
         //control
         public static List<Character> TheCharacters;
         public static List<Player> ThePlayers;
         public static Texture2D tx2PlayerIndicator;

         //guns
         public static List<Missile> TheMissiles;

         //characters
         public static CharacterTypes theCharacterTypes;

         //textures
         public static List<Texture2D> tx2Ground;
         public static List<Texture2D> tx2Wall;
         public static List<Texture2D> tx2EnemyBase = new List<Texture2D>();
         public static List<Texture2D> tx2DamagedEnemyBase = new List<Texture2D>();
         public static List<Texture2D> tx2HalfDamagedEnemyBase = new List<Texture2D>();
         public static List<Texture2D> tx2Enemy;


         public static Texture2D tx2Doors;
         public static Texture2D tx2Health;         

         public static Texture2D tx2Transporter;
         public static Texture2D tx2Computer;
         //level
         public static Level CurrentLevel;
         public static bool isLevelLoaded = false;
         //screen
         public static UIInGame _UIInGame;
         //Enemies
         public static List<Enemy> TheEnemyObjects;
         public static List<Enemy> lstEnemy = new List<Enemy>();
         //Usable
         public static List<MapHealth> lstHealth = new List<MapHealth>();
         public static List<MapDoors> lstDoor = new List<MapDoors>();

         //Debug
         public static bool StartEnemies = false;

         public static void Initialise()
         {
        

             //players
             ThePlayers = new List<Player>();
             //create player 


             //weapons
             TheMissiles = new List<Missile>();

             //enemies

             //characters
             string charTypesText = System.IO.File.ReadAllText(@".\Content\Specs\Characters.json");
             theCharacterTypes = JsonConvert.DeserializeObject<CharacterTypes>(charTypesText);

             TheCharacters = new List<Character>();
             //create character sprites
             Character Sky = AnimationSpecReader.PopulateAnimations<Character>(@".\Content\Specs\CharacterSpritesJson\Sky.json");
             Character Dirac = AnimationSpecReader.PopulateAnimations<Character>(@".\Content\Specs\CharacterSpritesJson\Dirac.json");
             Character Wanda = AnimationSpecReader.PopulateAnimations<Character>(@".\Content\Specs\CharacterSpritesJson\Wanda.json");
             Character Bartholomew = AnimationSpecReader.PopulateAnimations<Character>(@".\Content\Specs\CharacterSpritesJson\Bartholomew.json");
             //add characters to list
             TheCharacters.Add(Sky);
             TheCharacters.Add(Dirac);
             TheCharacters.Add(Wanda);
             TheCharacters.Add(Bartholomew);
             //fill in character details
             for (int i = 0; i < 4; i++)
             {
                 TheCharacters[i].iID = i;
                 TheCharacters[i].Name = theCharacterTypes.Types[i].sName;
                 TheCharacters[i].StreetSmart = theCharacterTypes.Types[i].iStreetSmarts;
                 TheCharacters[i].Hacking = theCharacterTypes.Types[i].iHacking;
                 TheCharacters[i].Speed = theCharacterTypes.Types[i].iSpeed;
                 TheCharacters[i].Stamina = theCharacterTypes.Types[i].iStamina;
                 TheCharacters[i].StandatGun = theCharacterTypes.Types[i].iStandartGunID;
                 TheCharacters[i].NormalGun = theCharacterTypes.Types[i].iNormalGunID;
                 TheCharacters[i].HeavyGun = theCharacterTypes.Types[i].iHeavyGunID;
                 TheCharacters[i].Bomb = theCharacterTypes.Types[i].iBombID;
             }


             TheEnemyObjects = new List<Enemy>();
             //textures
             tx2Ground = new List<Texture2D>();
             tx2Wall = new List<Texture2D>();
             tx2Enemy = new List<Texture2D>();


             //database 
             Database = new GameDatabase();

             //font
             MainFont = new FontRenderer("Fonts/galacticfont", 3);

             //screens
             _UIInGame = new UIInGame();
             //Players
             for (int i = 0; i < 4; i++)
             {
                 Player CurrentPlayer = new Player(i);
                 ThePlayers.Add(CurrentPlayer);

             }

         }

         public static void LoadLevel(int iLevelNumber)
         {
             isLevelLoaded = false;
             CurrentLevel = new Level();
             string text = System.IO.File.ReadAllText(@".\Content\Level\Level" + (iLevelNumber) + ".json");
             CurrentLevel = JsonConvert.DeserializeObject<Level>(text);

             CurrentLevel.Initialise();

             AIManager = new AIManager(CurrentLevel.EnemyBases);        
          
             
             isLevelLoaded = true;

         }

         public static void Update()
         {
             Level.Update();

             List<MapEnemyBase> EnemyBases = CurrentLevel.EnemyBases;

             if (StartEnemies)
             {         AIManager.Update();
                 
             }    
        
             for (int i = 0; i < 4; i++)
             {
                 if (ThePlayers[i].IsActive)
                     ThePlayers[i].Update();
                 else
                     ThePlayers[i].InactiveUpdate();
             }
             TheMissiles = TheMissiles.Where(item => item.DestroyMe == false).ToList();
             for (int i = 0; i < TheMissiles.Count; i++)
             {
                 TheMissiles[i].Update();
             }

             for (int i = 0; i < EnemyBases.Count; i++)
             {
                 if (StartEnemies)
                     EnemyBases[i].Update();
             }

             lstEnemy = lstEnemy.Where(item => item.DestroyMe == false).ToList();
             for (int i = 0; i < lstEnemy.Count; i++)
             {
                 lstEnemy[i].Update();
             }

             lstHealth.RemoveAll(item => item.bIsDirty == true);
             for (int i = 0; i < lstHealth.Count; i++)
             {
                 lstHealth[i].Update();
             }

             lstDoor.RemoveAll(item => item.DestroyMe == true);
             for (int i = 0; i < lstDoor.Count; i++)
             {
                 lstDoor[i].Update();
             }

             //screens
            _UIInGame.Update();
         }
         public static void Draw()
         {
             List<MapGrounds> Grounds = CurrentLevel.Grounds;
             List<MapWalls> Walls = CurrentLevel.Walls;
             List<MapEnemyBase> EnemyBases = CurrentLevel.EnemyBases;
             List<MapTransporter> Transporters = CurrentLevel.Transporters;
             lstHealth = CurrentLevel.Healths;
             lstDoor = CurrentLevel.Doors;           

             List<MapComputer> Computers = CurrentLevel.Computer;

             for (int i = 0; i < Walls.Count; i++)
             {
                 Walls[i].Draw();
             }
             
             for (int i = 0; i < Transporters.Count; i++)
             {
                 Transporters[i].Draw();
             }
             for (int i = 0; i < Grounds.Count; i++)
             {
                 Grounds[i].Draw();
             }
            
             for (int i = 0; i < lstHealth.Count; i++)
             {
                 lstHealth[i].Draw();
             }
             for (int i = 0; i < Computers.Count; i++)
             {
                 Computers[i].Draw();
             }
             for (int i = 0; i < EnemyBases.Count; i++)
             {
                 EnemyBases[i].Draw();
             }          

             for (int i = 0; i < lstDoor.Count; i++ )
             {
                 lstDoor[i].Draw();
             }

             for (int i = 0; i < TheMissiles.Count; i++)
             {
                 TheMissiles[i].Draw();
             }

             for (int i = 0; i < lstEnemy.Count; i++)
             {
                 lstEnemy[i].Draw();
             }
             for (int i = 0; i < 4; i++)
             {
                 if (ThePlayers[i].IsActive)
                     ThePlayers[i].Draw();
             }
         }
         public static void DrawUI()
         {

             _UIInGame.Draw();
         }


     }
 }

