
﻿using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StellarRats
{
    class Debug
    {

       
        static KeyboardState kOldState;
        public static void Update()
        { 
            if(Keyboard.GetState().IsKeyUp(Keys.D1) && kOldState.IsKeyDown(Keys.D1))
            {
                GameStatic.ThePlayers[0].IsActive = !GameStatic.ThePlayers[0].IsActive;
            }
            if (Keyboard.GetState().IsKeyUp(Keys.D2) && kOldState.IsKeyDown(Keys.D2))
            {
                GameStatic.ThePlayers[1].IsActive = !GameStatic.ThePlayers[1].IsActive;

            }
            if (Keyboard.GetState().IsKeyUp(Keys.D3) && kOldState.IsKeyDown(Keys.D3))
            {
                GameStatic.ThePlayers[2].IsActive = !GameStatic.ThePlayers[2].IsActive;

            }
            if (Keyboard.GetState().IsKeyUp(Keys.D4) && kOldState.IsKeyDown(Keys.D4))
            {
                GameStatic.ThePlayers[3].IsActive = !GameStatic.ThePlayers[3].IsActive;

            }

            if (Keyboard.GetState().IsKeyUp(Keys.E) && kOldState.IsKeyDown(Keys.E))
            {
                GameStatic.StartEnemies = true; ;

            }

            kOldState = Keyboard.GetState();




            if (GameStatic.ThePlayers[2].IsActive)
            {
                if (Keyboard.GetState().IsKeyDown(Keys.Up))
                {
                    GameStatic.ThePlayers[2].Position.Y -= 3;
                }
                if (Keyboard.GetState().IsKeyDown(Keys.Down))
                {
                    GameStatic.ThePlayers[2].Position.Y += 3;
                }
                if (Keyboard.GetState().IsKeyDown(Keys.Left))
                {
                    GameStatic.ThePlayers[2].Position.X -= 3;
                }
                if (Keyboard.GetState().IsKeyDown(Keys.Right))
                {
                    GameStatic.ThePlayers[2].Position.X += 3;
                }
            }

            if (GameStatic.ThePlayers[3].IsActive)
            {
                if (Keyboard.GetState().IsKeyDown(Keys.W))
                {
                    GameStatic.ThePlayers[3].Position.Y -= 3;
                }
                if (Keyboard.GetState().IsKeyDown(Keys.S))
                {
                    GameStatic.ThePlayers[3].Position.Y += 3;
                }
                if (Keyboard.GetState().IsKeyDown(Keys.A))
                {
                    GameStatic.ThePlayers[3].Position.X -= 3;
                }
                if (Keyboard.GetState().IsKeyDown(Keys.D))
                {
                    GameStatic.ThePlayers[3].Position.X += 3;
                }
            }

        }
    }
}

