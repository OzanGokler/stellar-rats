﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Stellar_Rats.GameObjects
{
    public static class InputController
    {
        public static Keys GetDirectionByPlayer(PlayerIndex index, Direction dir)
        {
            switch (dir)
            {
                case Direction.Left:
                    break;
                case Direction.Right:
                    break;
                case Direction.Up:
                    break;
                case Direction.Down:
                    break;
                default:
                    break;
            }

            return Keys.D6;
        } 
    }

    public enum Direction
    {
        Left,
        Right,
        Up,
        Down
    }
}
