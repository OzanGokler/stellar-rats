﻿using HologramSpriteManager;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StellarRats
{
    class Player 
    {
        public Vector2 Position;
        public  Character ThisCharacter;
        public Color PlayerColor;
        public List<MapComputer> mapComputerList = new List<MapComputer>();
        public List<Collision> mapComputerColList = new List<Collision>();
        public Hack hacking = new Hack();
        public Stamina stamina = new Stamina();
        GamePadState oldGampePadState;

        PlayerIndex _Index;
        public bool IsActive = false;
        public int changeCharacter = 0;
        public int ID;
        //0-StandatdGun 1-NormalGun 2-HeavyGun 3-Bomb for GunID
        public int UsingGunID = 0;
        public int GunCounter = 0;
        public bool playerIsActive = false;
        public Player(int Index)
        {
            _Index = PlayerIndex.One + Index;
            ID = Index;
            //x = GameStatic.CurrentLevel.Transporters[0].X;
            //y = GameStatic.CurrentLevel.Transporters[0].Y;
            ThisCharacter = GameStatic.TheCharacters[ID];

            StellarRatsGame.hackbarpositon = new Vector2(ThisCharacter.Position.X, ThisCharacter.Position.Y);
            if (Index == 0)
            {
                PlayerColor = Color.Red;
            }
            if (Index == 1)
            {
                PlayerColor = Color.Blue;
            }
            if (Index == 2)
            {
                PlayerColor = Color.Yellow;
            }
            if (Index == 3)
            {
                PlayerColor = Color.Green;
            }
            playerIsActive = true;
        }

       

        
        
        //Usable
        public bool bDoors = true;

        //Guns Variables
        public float fLastMissileFire = 0;
        public string GunName = "";
        public float GunRefireRate = 0;
        public int GunPower = 0;
        public string GunAmmo = "";
        public void Draw()
        {
            PlayerColorDraw();
            ThisCharacter.Draw();
            hacking.HackBarDraw(ThisCharacter);
  
        }

        public void PlayerColorDraw()
        {
            SpriteManager.spriteBatch.Draw(GameStatic.tx2PlayerIndicator, new Rectangle(Convert.ToInt32(Position.X),Convert.ToInt32(Position.Y),100,100), PlayerColor);
        }

        public void InactiveUpdate()
        {
            GamePadState gamePadState = GamePad.GetState(_Index);
             if (oldGampePadState.IsButtonDown(Buttons.Start) && gamePadState.IsButtonUp(Buttons.Start))
             {
                 Position.X = GameStatic.CurrentLevel.Transporters[0].X;
                 Position.Y = GameStatic.CurrentLevel.Transporters[0].Y;
                 ThisCharacter.Position = Position;
                 GetNextCharacter();
                 IsActive = !GameStatic.ThePlayers[ID].IsActive;
             }


             oldGampePadState = gamePadState;        
        }

        
        public void Update()
        {
            GamePadState gamePadState = GamePad.GetState(_Index);
            float fCurrentTime = SpriteManager.GameTime;
            float fMissileTimePassed = fCurrentTime - fLastMissileFire;
            stamina.fCharacterSpeed = ThisCharacter.Speed;
            CalculateGunSpecials(UsingGunID);

            if (IsActive)
            {
                //MapHealthCollision
                for (int i = 0; i < GameStatic.CurrentLevel.Healths.Count; i++)
                {

                    MapHealth CurrentMapHealth = GameStatic.CurrentLevel.Healths[i];
                    Collision currentCollision = ThisCharacter.CheckPixelCollision(CurrentMapHealth.Bounds());
                    if (currentCollision.bCollided)
                    {
                        Console.WriteLine("this is aspirine!");
                        CurrentMapHealth.bIsDirty = true;
                        ThisCharacter.Health += CurrentMapHealth.iHealthPower;
                        
                    }
                }


                if (oldGampePadState.IsButtonDown(Buttons.Y) && gamePadState.IsButtonUp(Buttons.Y))
                {
                    GunCounter++;
                    if (GunCounter == 3)
                        GunCounter = 0;

                    switch (GunCounter)
                    {
                        case 0:
                            UsingGunID = ThisCharacter.StandatGun;
                            break;
                        case 1:
                            UsingGunID = ThisCharacter.NormalGun;
                            break;
                        case 2:
                            UsingGunID = ThisCharacter.HeavyGun;
                            break;
                    }
                    CalculateGunSpecials(UsingGunID);
                    Console.WriteLine("Gun Name: " + GunName);
                    Console.WriteLine("Gun Power: " + GunPower);
                    Console.WriteLine("Gun RefireRate: " + GunRefireRate);
                    Console.WriteLine("Gun AmmoSet: " + GunAmmo);
                    Console.WriteLine("\n\n");
                }


                if (oldGampePadState.IsButtonDown(Buttons.A) && gamePadState.IsButtonUp(Buttons.A))
                {
                    GetNextCharacter();
                    Console.WriteLine("Char Name: " + ThisCharacter.Name);
                    Console.WriteLine("Char Street Smart: " + ThisCharacter.StreetSmart);
                    Console.WriteLine("Char Hacking: " + ThisCharacter.Hacking);
                    Console.WriteLine("Char Speed: " + ThisCharacter.Speed);
                    Console.WriteLine("\n\n");
                }
                

                if (gamePadState.Triggers.Right > 0.3f)
                {
                    if ((fMissileTimePassed / GunRefireRate) > 1)
                    {
                        Missile missile = new Missile(GameDatabase._Missiles.Clone());
                        missile.Direction = ThisCharacter.Direction;
                        if(ThisCharacter.Direction == Character.DirectionState.Right)
                        {
                            missile.Position.X = Position.X + 75;
                            missile.Position.Y = Position.Y + 35;
                        }
                        if (ThisCharacter.Direction == Character.DirectionState.Left)
                        {
                            missile.Position.X = Position.X;
                            missile.Position.Y = Position.Y + 35;
                        }
                        if (ThisCharacter.Direction == Character.DirectionState.Up)
                        {
                            missile.Position.X = Position.X + 35;
                            missile.Position.Y = Position.Y ;
                        }
                        if (ThisCharacter.Direction == Character.DirectionState.Down)
                        {
                            missile.Position.X = Position.X + 35;
                            missile.Position.Y = Position.Y + 75;
                        }
                        if (ThisCharacter.Direction == Character.DirectionState.UpRight)
                        {
                            missile.Position.X = Position.X + 75;
                            missile.Position.Y = Position.Y;
                        }
                        if (ThisCharacter.Direction == Character.DirectionState.UpLeft)
                        {
                            missile.Position.X = Position.X;
                            missile.Position.Y = Position.Y;
                        }
                        if (ThisCharacter.Direction == Character.DirectionState.DownRight)
                        {
                            missile.Position.X = Position.X + 75;
                            missile.Position.Y = Position.Y + 75;
                        }
                        if (ThisCharacter.Direction == Character.DirectionState.DownLeft)
                        {
                            missile.Position.X = Position.X;
                            missile.Position.Y = Position.Y + 75;
                        }
                        
                        missile.iFiredPlayer = ID;
                        missile.iDamage = GunPower;
                        
                        GameStatic.TheMissiles.Add(missile);
                        fLastMissileFire = fCurrentTime;
                    }
                }


                if (gamePadState.IsButtonDown(Buttons.B))
                {
                    if (hacking.bHacking == false)
                    {
                        hacking.fComputerHackStart = fCurrentTime;
                        hacking.fComputerHackPeriod = ThisCharacter.Hacking;
                        hacking.bHacking = true;
                    }
                    hacking.Hacking(fCurrentTime, ThisCharacter);
                  
                }
                if (gamePadState.IsButtonUp(Buttons.B) && oldGampePadState.IsButtonDown(Buttons.B))
                {

                    hacking.bHacking = false;
                    hacking.bHackSelectedComp = false;
                }

                if (gamePadState.IsButtonDown(Buttons.X))
                {
                    if (stamina.bStamina == false)
                    {
                        stamina.fStaminaStartTime = fCurrentTime;
                        stamina.fTotalRunTime = fCurrentTime;
                        stamina.bStamina = true;
                        stamina.fRunTimePast = stamina.fRunTime;
                    }
                    if (stamina.fRunTime > ThisCharacter.Stamina)
                    {
                        //fRunTime = ThisCharacter.Stamina;
                        stamina.bStamina = false;                       
                    }

                    stamina.fRunTime = fCurrentTime - stamina.fStaminaStartTime + stamina.fRunTimePast;
                    stamina._Stamina(stamina.fRunTime, ThisCharacter);
                }
                if (gamePadState.IsButtonUp(Buttons.X) && oldGampePadState.IsButtonDown(Buttons.X))
                {
                    stamina.fRunTimeEnd = fCurrentTime;
                    stamina.fTotalRunTime = fCurrentTime + (fCurrentTime - stamina.fTotalRunTime);

                }
                if (gamePadState.IsButtonUp(Buttons.X))
                {
                    stamina.bStamina = false;
                    if (stamina.fRunTime > 0)
                    {
                        stamina.fRunTime = stamina.fTotalRunTime - (fCurrentTime) + stamina.fRunTimePast;
                        if (stamina.fRunTime < 0)
                        {
                            stamina.fRunTime = 0;
                        }                       
                    }
                    
                }

                if (gamePadState.ThumbSticks.Left.X > 0.3f && ThisCharacter.CanMove(Character.DirectionState.Right))
                {
                    Position.X += stamina.fCharacterSpeed;
                    ThisCharacter.Direction = Character.DirectionState.Right;
                    ThisCharacter.ChangeAnimation("right", false);
                }
                if (gamePadState.ThumbSticks.Left.X < -0.3f && ThisCharacter.CanMove(Character.DirectionState.Left))
                {
                    Position.X -= stamina.fCharacterSpeed;
                    ThisCharacter.Direction = Character.DirectionState.Left;
                    ThisCharacter.ChangeAnimation("left", false);
                }
                if (gamePadState.ThumbSticks.Left.Y > 0.3f && ThisCharacter.CanMove(Character.DirectionState.Up))
                {
                    Position.Y -= stamina.fCharacterSpeed;
                    ThisCharacter.Direction = Character.DirectionState.Up;
                    ThisCharacter.ChangeAnimation("up", false);
                }
                if (gamePadState.ThumbSticks.Left.Y < -0.3f && ThisCharacter.CanMove(Character.DirectionState.Down))
                {
                    Position.Y += stamina.fCharacterSpeed;
                    ThisCharacter.Direction = Character.DirectionState.Down;
                    ThisCharacter.ChangeAnimation("down", false);
                }
                if (gamePadState.ThumbSticks.Left.Y < -0.3f && gamePadState.ThumbSticks.Left.X > 0.3f)
                {
                    ThisCharacter.Direction = Character.DirectionState.DownRight;
                    ThisCharacter.ChangeAnimation("downright", false);
                }
                if (gamePadState.ThumbSticks.Left.Y < -0.3f && gamePadState.ThumbSticks.Left.X < -0.3f)
                {
                    ThisCharacter.Direction = Character.DirectionState.DownLeft;
                    ThisCharacter.ChangeAnimation("downleft", false);
                }
                if (gamePadState.ThumbSticks.Left.Y > 0.3f && gamePadState.ThumbSticks.Left.X > 0.3f)
                {
                    ThisCharacter.Direction = Character.DirectionState.UpRight;
                    ThisCharacter.ChangeAnimation("upright", false);
                }
                if (gamePadState.ThumbSticks.Left.Y > 0.3f && gamePadState.ThumbSticks.Left.X < -0.3f)
                {
                    ThisCharacter.Direction = Character.DirectionState.UpLeft;
                    ThisCharacter.ChangeAnimation("upleft", false);
                }


                if(gamePadState.ThumbSticks.Right.X > 0.3f && ThisCharacter.CanMove(Character.DirectionState.Right))
                {
                    ThisCharacter.Direction = Character.DirectionState.Right;
                    ThisCharacter.ChangeAnimation("right", false);
                }
                if(gamePadState.ThumbSticks.Right.X < -0.3f && ThisCharacter.CanMove(Character.DirectionState.Left))
                {
                    ThisCharacter.Direction = Character.DirectionState.Left;
                    ThisCharacter.ChangeAnimation("left", false);
                }
                if(gamePadState.ThumbSticks.Right.Y > 0.3f && ThisCharacter.CanMove(Character.DirectionState.Up))
                {
                    ThisCharacter.Direction = Character.DirectionState.Up;
                    ThisCharacter.ChangeAnimation("up", false);
                }
                if(gamePadState.ThumbSticks.Right.Y < -0.3f && ThisCharacter.CanMove(Character.DirectionState.Down))
                {
                    ThisCharacter.Direction = Character.DirectionState.Down;
                    ThisCharacter.ChangeAnimation("down", false);
                }
                if(gamePadState.ThumbSticks.Right.Y > 0.3f && gamePadState.ThumbSticks.Right.X > 0.3f && ThisCharacter.CanMove(Character.DirectionState.UpRight))
                {
                    ThisCharacter.Direction = Character.DirectionState.UpRight;
                    ThisCharacter.ChangeAnimation("upright", false);
                }
                if(gamePadState.ThumbSticks.Right.Y > 0.3f && gamePadState.ThumbSticks.Right.X < -0.3f && ThisCharacter.CanMove(Character.DirectionState.UpLeft))
                {
                    ThisCharacter.Direction = Character.DirectionState.UpLeft;
                    ThisCharacter.ChangeAnimation("upleft", false);
                }
                if(gamePadState.ThumbSticks.Right.Y < -0.3f && gamePadState.ThumbSticks.Right.X > 0.3f && ThisCharacter.CanMove(Character.DirectionState.DownRight))
                {
                    ThisCharacter.Direction = Character.DirectionState.DownRight;
                    ThisCharacter.ChangeAnimation("downright", false);
                }
                if (gamePadState.ThumbSticks.Right.Y < -0.3f && gamePadState.ThumbSticks.Right.X < -0.3f && ThisCharacter.CanMove(Character.DirectionState.DownLeft))
                {
                    ThisCharacter.Direction = Character.DirectionState.DownLeft;
                    ThisCharacter.ChangeAnimation("downleft", false);
                }
                oldGampePadState = gamePadState;
            }
            ThisCharacter.Position = Position;        
        }

        private void GetNextCharacter()
        {
            bool bIsStandingOnTeleporter = false;
            bool bSelected = false;
            int iNextCharacter = ThisCharacter.iID;

            for (int i = 0; i < GameStatic.CurrentLevel.Transporters.Count;i++ )
            {
                MapTransporter currentTransporter = GameStatic.CurrentLevel.Transporters[i];
                Collision currentCollision = ThisCharacter.CheckPixelCollision(currentTransporter.Bounds());
                if (currentCollision.bCollided)
                {
                    bIsStandingOnTeleporter = true;
                    stamina.fRunTime = 0;
                    break;
                }
            }
            if (!bIsStandingOnTeleporter)
                return;

            
            while (bSelected == false)
            {
                iNextCharacter++;
                if (iNextCharacter == GameStatic.TheCharacters.Count())
                {
                    iNextCharacter = 0;
                }

                //check if character in use
                int iCheckedPlayerCounter = 0;
                for (int i = 0; i < 4; i++)
                {
                    Player CurrentPlayer = GameStatic.ThePlayers[i];
                    if (CurrentPlayer.ID != ID)
                    {
                        if  (!CurrentPlayer.IsActive)
                        {
                            iCheckedPlayerCounter++;

                        }
                        else if (CurrentPlayer.ThisCharacter.iID != GameStatic.TheCharacters[iNextCharacter].iID)
                        {
                            iCheckedPlayerCounter++;
                        }
                    }
                }
                if(iCheckedPlayerCounter==3)
                {
                    GameStatic.TheCharacters[iNextCharacter].Position = Position;
                    ThisCharacter = GameStatic.TheCharacters[iNextCharacter];
                    bSelected = true;
                }
            }
        }

        public void CharacterInAndOut()
        {

            for (int i = 0; i < GameStatic.CurrentLevel.Transporters.Count;i++ )
            {
                MapTransporter currentTransporter = GameStatic.CurrentLevel.Transporters[i];
                Collision currentCollision = ThisCharacter.CheckPixelCollision(currentTransporter.Bounds());
                if (currentCollision.bCollided)
                {                  
                    break;
                }
            }

            GameStatic.ThePlayers[ThisCharacter.iID].IsActive = !GameStatic.ThePlayers[ThisCharacter.iID].IsActive;
        }

        public void CalculateGunSpecials(int GunID)
        {
            GunName = GameDatabase._GunTypes.Types[GunID].name;
            GunPower = GameDatabase._GunTypes.Types[GunID].power;
            GunRefireRate = GameDatabase._GunTypes.Types[GunID].refirerate;
            GunAmmo = GameDatabase._GunTypes.Types[GunID].ammo;
        }
       
       
    }
}