﻿using HologramSpriteManager;
using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StellarRats
{
    class Hack
    {
        //Hack Variables
        public float fComputerHackStart = 0;
        public float fComputerHackPeriod = 0;
        public bool bHacking = false;
        public bool bHackSelectedComp = false;
        public int rHackBarWidth = 254;
        int selectedCompID = -1;
        Rectangle r = new Rectangle();

        public void Hacking(float Time, Character ThisCharacter)
        {
            //Collision Computer
            if (bHackSelectedComp == false)
            {
                for (int i = 0; i < GameStatic.CurrentLevel.Computer.Count; i++)
                {
                    MapComputer currentComputer = GameStatic.CurrentLevel.Computer[i];
                    Collision currentCollision = ThisCharacter.CheckPixelCollision(currentComputer.Bounds());
                    if (currentCollision.bCollided)
                    {
                        bHackSelectedComp = true;
                        selectedCompID = currentComputer.ID;
                    }
                    else
                    {
                        bHacking = false;
                    }
                }
            }
            if (bHackSelectedComp == true)
            {
                if (fComputerHackStart + fComputerHackPeriod >= Time)
                {
                    Console.WriteLine("Hacking");
                    float fNewWidth = ((Time - fComputerHackStart) / fComputerHackPeriod) * rHackBarWidth;
                    Console.WriteLine("ID: " + ThisCharacter.iID);
                    r = StellarRatsGame.hackbarRectangle;
                    r.Width = (int)fNewWidth;
                }
                else
                {
                    Console.WriteLine("Hack Complete");
                    for (int j = 0; j < GameStatic.CurrentLevel.Doors.Count; j++)
                    {
                        MapDoors currentDoor = GameStatic.CurrentLevel.Doors[j];
                        DoorCollider currentDoorCollider = GameStatic.CurrentLevel.DoorColliders[j];
                        if (selectedCompID == currentDoor.ID)
                        {
                            currentDoorCollider.bDestroyMe = true;
                            currentDoor.DestroyMe = true;
                        }
                    }
                }
            }

        }

        public void HackBarDraw(Character ThisCharacter)
        {
            if (bHacking)
            {
                SpriteManager.spriteBatch.Draw(StellarRats.StellarRatsGame.tx2hackBarEnd, ThisCharacter.Position, StellarRats.StellarRatsGame.hackbarRectangleEnd, Color.White);
                SpriteManager.spriteBatch.Draw(StellarRats.StellarRatsGame.tx2hackBar, ThisCharacter.Position, r, Color.White);

            }
        }
    }
}
