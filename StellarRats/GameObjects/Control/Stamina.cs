﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StellarRats
{
    class Stamina
    {
        //Stamina Variables
        public float fStaminaStartTime = 0;
        public bool bStamina = false;
        public float fStaminaSpeed = 0;
        public float fCharacterSpeed = 0;
        public float fRunTime = 0;
        public float fRestTimeEnd = 0;
        public float fRunTimeEnd = 0;
        public float fTotalRunTime = 0;
        public float fRunTimePast = 0;

        public void _Stamina(float RunTime, Character ThisCharacter)
        {

            if (RunTime <= ThisCharacter.Stamina)
            {
                fCharacterSpeed = ThisCharacter.Speed + 5;
            }
            else
            {
                fCharacterSpeed = ThisCharacter.Speed;
            }
        }
    }
}
