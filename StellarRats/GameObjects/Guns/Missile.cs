﻿using HologramSpriteManager;
using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StellarRats
{
    class Missile : AnimatedSprite
    {
        public int iFiredPlayer = 0;

        float StartTime;

        public Missile(AnimatedSpriteSequences Meta)
            : base(Meta)
        {
            
        }
        public Missile(AnimatedSprite Clone)
        {
            Sequences = Clone.Sequences;
            StartTime = SpriteManager.GameTime;

        }

        public Character.DirectionState Direction = Character.DirectionState.Right;

        public int iDamage = 1;
        public int iSpeed = 35;
        public float FinishTime = 2000;
        public bool DestroyMe = false;

        public void Update()
        {
            if (Direction == Character.DirectionState.Right)
            {
                Position.X += iSpeed;
            }
            if (Direction == Character.DirectionState.Left)
            {
                Position.X -= iSpeed;
            }
            if (Direction == Character.DirectionState.Up)
            {
                Position.Y -= iSpeed;
            }
            if (Direction == Character.DirectionState.Down)
            {
                Position.Y += iSpeed;
            }
            if (Direction == Character.DirectionState.UpRight)
            {
                Position.Y -= iSpeed / (float)Math.Sqrt(2);
                Position.X += iSpeed / (float)Math.Sqrt(2);
            }
            if (Direction == Character.DirectionState.UpLeft)
            {
                Position.Y -= iSpeed / (float)Math.Sqrt(2);
                Position.X -= iSpeed / (float)Math.Sqrt(2);
            }
            if (Direction == Character.DirectionState.DownRight)
            {
                Position.Y += iSpeed / (float)Math.Sqrt(2);
                Position.X += iSpeed / (float)Math.Sqrt(2);
            }
            if (Direction == Character.DirectionState.DownLeft)
            {
                Position.Y += iSpeed / (float)Math.Sqrt(2);
                Position.X -= iSpeed / (float)Math.Sqrt(2);
            }

            //collide players 
            for (int i = 0; i < 4; i++)
            {

                Player CurrentPlayer = GameStatic.ThePlayers[i];
                if (CurrentPlayer.IsActive && iFiredPlayer !=i)
                {
                    Collision currentCollision = CheckPixelCollision(CurrentPlayer.ThisCharacter);
                    if (currentCollision.bCollided)
                    {
                        CurrentPlayer.ThisCharacter.Hit(iDamage);
                        DestroyMe = true;
                    }
                }
            }
            //collide walls
            for (int i = 0; i < GameStatic.CurrentLevel.WallColliders.Count; i++)
            {

                WallCollider CurrentWall = GameStatic.CurrentLevel.WallColliders[i];
                Collision currentCollision = CheckPixelCollision(CurrentWall.TotalRectangle);
                if (currentCollision.bCollided)
                {
                    DestroyMe = true;
                }
            }
            //Collide doors
            for (int i = 0; i < GameStatic.CurrentLevel.DoorColliders.Count; i++)
            {
                DoorCollider CurrentDoor = GameStatic.CurrentLevel.DoorColliders[i];
                Collision currentCollision = CheckPixelCollision(CurrentDoor.TotalRectangle);
                if (currentCollision.bCollided)
                {
                    DestroyMe = true;
                }
            }

            //collide enemy
            for (int i = 0; i < GameStatic.lstEnemy.Count; i++)
            {

                Enemy CurrentEnemy = GameStatic.lstEnemy[i];
                Collision currentCollision = CheckPixelCollision(CurrentEnemy);
                if (currentCollision.bCollided)
                {
                    CurrentEnemy.Hit(iDamage);
                    DestroyMe = true;
                }
            }
            //collide base 
            List<MapEnemyBase> EnemyBases = GameStatic.CurrentLevel.EnemyBases;
            for (int i = 0; i <EnemyBases.Count; i++)
            {

                MapEnemyBase CurrentBase = EnemyBases[i];
                Collision currentCollision = CheckPixelCollision(new Rectangle (CurrentBase.X,CurrentBase.Y,50,50));
                if (currentCollision.bCollided)
                {
                    CurrentBase.Hit(iDamage);
                    DestroyMe = true;
                }
            }

            if (SpriteManager.GameTime > StartTime +FinishTime)
            {
                DestroyMe = true;
            }
        }
    }
}