﻿using HologramSpriteManager;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StellarRats
{
    public class Character : AnimatedSprite
    {
       public Character(AnimatedSpriteSequences Meta)
            : base(Meta)
        {
            
        }
        public int Health = 100;
        public int iID = 0;
        public string Name = "";
        public int StreetSmart = 0;
        public int Hacking = 0;
        public float Speed = 0;
        public int Stamina = 0;
        public int StandatGun = 0;
        public int NormalGun = 0;
        public int HeavyGun = 0;
        public int Bomb = 0;

        public enum DirectionState
        {
            Up,
            Down,
            Left,
            Right,
            UpLeft,
            UpRight,
            DownLeft,
            DownRight
        }
        public DirectionState Direction = DirectionState.Right;      
        

        public void Hit(int Damage)
        {

            Health -= Damage;
        }

        public bool CanMove(DirectionState ProposedDirection)
        {
            for (int i = 0; i < GameStatic.CurrentLevel.WallColliders.Count; i++)
            {
                WallCollider CurrentCollider = GameStatic.CurrentLevel.WallColliders[i];

                if (ProposedDirection == DirectionState.Down || ProposedDirection == DirectionState.DownLeft || ProposedDirection == DirectionState.DownRight)
                {
                    Collision currentCollision = CheckPixelCollision(CurrentCollider.rTop);
                    if (currentCollision.bCollided)
                    {
                        return false;
                    }
                }

                if (ProposedDirection == DirectionState.Up || ProposedDirection == DirectionState.UpLeft || ProposedDirection == DirectionState.UpRight)
                {

                    Collision currentCollision = CheckPixelCollision(CurrentCollider.rBottom);
                    if (currentCollision.bCollided)
                    {
                        return false;
                    }
                }

                if (ProposedDirection == DirectionState.Right || ProposedDirection == DirectionState.UpRight || ProposedDirection == DirectionState.DownRight)
                {

                    Collision currentCollision = CheckPixelCollision(CurrentCollider.rLeft);
                    if (currentCollision.bCollided)
                    {
                        return false;
                    }
                }
                if (ProposedDirection == DirectionState.Left || ProposedDirection == DirectionState.UpLeft || ProposedDirection == DirectionState.DownLeft)
                {

                    Collision currentCollision = CheckPixelCollision(CurrentCollider.rRight);
                    if (currentCollision.bCollided)
                    {
                        return false;
                    }
                }
            }

            for (int i = 0; i < GameStatic.CurrentLevel.DoorColliders.Count; i++)
            {
                DoorCollider CurrentCollider = GameStatic.CurrentLevel.DoorColliders[i];

                if (ProposedDirection == DirectionState.Down || ProposedDirection == DirectionState.DownLeft || ProposedDirection == DirectionState.DownRight)
                {
                    Collision currentCollision = CheckPixelCollision(CurrentCollider.rTop);
                    if (currentCollision.bCollided)
                    {
                        return false;
                    }
                }

                if (ProposedDirection == DirectionState.Up || ProposedDirection == DirectionState.UpLeft || ProposedDirection == DirectionState.UpRight)
                {

                    Collision currentCollision = CheckPixelCollision(CurrentCollider.rBottom);
                    if (currentCollision.bCollided)
                    {
                        return false;
                    }
                }

                if (ProposedDirection == DirectionState.Right || ProposedDirection == DirectionState.UpRight || ProposedDirection == DirectionState.DownRight)
                {

                    Collision currentCollision = CheckPixelCollision(CurrentCollider.rLeft);
                    if (currentCollision.bCollided)
                    {
                        return false;
                    }
                }
                if (ProposedDirection == DirectionState.Left || ProposedDirection == DirectionState.UpLeft || ProposedDirection == DirectionState.DownLeft)
                {

                    Collision currentCollision = CheckPixelCollision(CurrentCollider.rRight);
                    if (currentCollision.bCollided)
                    {
                        return false;
                    }
                }
            }

            return true;
        }

    }
}
