﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StellarRats
{
    class CharacterData
    {
        public int iID = 0;
        public string sName = "";
        public int iStreetSmarts = 0;
        public int iHacking = 0;
        public int iSpeed = 0;
        public int iStamina = 0;
        public int iStandartGunID = 0;
        public int iNormalGunID = 0;
        public int iHeavyGunID = 0;
        public int iBombID = 0;
    }
}
