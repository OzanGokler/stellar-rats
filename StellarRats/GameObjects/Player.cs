﻿
using HologramSpriteManager;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StellerRats.Player
{
    class Player : AnimatedSprite
    {

        public Player(AnimatedSpriteSequences Meta)
            : base(Meta)
        {
            Position = new Vector2(400, 300);
        }


        public void Update()
        {
            GamePadState gamePadState = GamePad.GetState(PlayerIndex.One);
            if (Keyboard.GetState().IsKeyDown(Keys.Up))
            {
                Position.Y -= 3;
                ChangeAnimation("idle");
            }
             if (Keyboard.GetState().IsKeyDown(Keys.Down))
            {
                Position.Y += 3;
                ChangeAnimation("idle");
            }
            if (Keyboard.GetState().IsKeyDown(Keys.Left))
            {
                Position.X -= 3;
                ChangeAnimation("idle");
            }
             if (Keyboard.GetState().IsKeyDown(Keys.Right))
            {
                Position.X += 3;
                ChangeAnimation("idle");
            }

        }
    }
}
