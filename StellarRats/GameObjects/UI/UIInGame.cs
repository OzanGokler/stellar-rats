﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StellarRats
{
    class UIInGame
    {
        public void Update()
        { }
        public void Draw()
        {
            //player player health
            GameStatic.MainFont.DrawTextWithWidth(new Vector2(50, 50), 1000, "Player1 health " + GameStatic.TheCharacters[0].Health + "  Position " + GameStatic.TheCharacters[0].Position.X.ToString() + " " + GameStatic.TheCharacters[0].Position.Y.ToString(), 1, Color.Green);
            GameStatic.MainFont.DrawTextWithWidth(new Vector2(50, 100), 1000, "Player2 health " + GameStatic.TheCharacters[1].Health + "  Position " + GameStatic.TheCharacters[1].Position.X.ToString() + " " + GameStatic.TheCharacters[1].Position.Y.ToString(), 1, Color.Green);
            GameStatic.MainFont.DrawTextWithWidth(new Vector2(50, 150), 1000, "Player3 health " + GameStatic.TheCharacters[2].Health, 1, Color.Green);
            GameStatic.MainFont.DrawTextWithWidth(new Vector2(50, 200), 1000, "Player4 health " + GameStatic.TheCharacters[3].Health, 1, Color.Green);
            GameStatic.MainFont.DrawTextWithWidth(new Vector2(50, 250), 1000, "Camera " + GameStatic.camera.TopLeft.X + "," + GameStatic.camera.TopLeft.Y, 1, Color.Green);
            //GameStatic.MainFont.DrawTextWithWidth(new Vector2(50, 300), 1000, "RestTime: " + (int)Player.fRunTime, 1, Color.Green);

        }
    }
}
