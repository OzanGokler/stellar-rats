﻿using HologramSpriteManager;
using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StellarRats
{
 class MapWalls
    {
        public int X;
        public int Y;
        public int Type;

        public void Draw()
        {
            Rectangle rWall = new Rectangle(X, Y, 50, 50);
            SpriteManager.spriteBatch.Draw(GameStatic.tx2Wall[Type], rWall, Color.White);
        }
    }
}
