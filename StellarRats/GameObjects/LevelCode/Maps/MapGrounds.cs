﻿using HologramSpriteManager;
using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StellarRats
{
    class MapGrounds
    {
        public int X;
        public int Y;
        public int Type;
         public void Draw()
        {

            Rectangle rGround = new Rectangle(X,Y, 50, 50);
            SpriteManager.spriteBatch.Draw(GameStatic.tx2Ground[Type], rGround, Color.White);
        }
    }
}
