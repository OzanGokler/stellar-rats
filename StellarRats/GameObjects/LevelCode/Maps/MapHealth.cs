﻿using HologramSpriteManager;
using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StellarRats
{
    class MapHealth : AnimatedSprite
    {
        public int X;
        public int Y;
        public int Height;
        public int Width;
        public int iHealthPower;
        public Rectangle rHealth;
        public bool bIsDirty = false;


        public void Draw()
        {
            rHealth = new Rectangle(X, Y, 30, 30);
            SpriteManager.spriteBatch.Draw(GameStatic.tx2Health, rHealth, Color.White);
        }

        public Rectangle Bounds()
        {
            return new Rectangle(X, Y, Width, Height);
        }

        public void Update()
        {
          
        }
    }
}