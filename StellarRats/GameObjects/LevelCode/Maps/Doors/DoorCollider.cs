﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StellarRats
{
    class DoorCollider
    {
        public int X;
        public int Y;
        public int Height;
        public int Width;
        public int ID;

        public int SizeBuffer = 10;

        public Rectangle rTop;
        public Rectangle rBottom;
        public Rectangle rLeft;
        public Rectangle rRight;

        public Rectangle TotalRectangle;
        public bool bDestroyMe = false;
        public void SetRectangles()
        {
            rTop = new Rectangle(X, Y, Width, SizeBuffer);
            rBottom = new Rectangle(X, Y + Height - SizeBuffer, Width, SizeBuffer);
            rLeft = new Rectangle(X, Y, SizeBuffer, Height);
            rRight = new Rectangle(X + Width-SizeBuffer, Y, SizeBuffer,Height);
            TotalRectangle = new Rectangle(X,Y,Width,Height);
        }


    }
}
