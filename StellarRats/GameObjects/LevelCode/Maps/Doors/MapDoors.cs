﻿using HologramSpriteManager;
using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StellarRats
{
 class MapDoors : AnimatedSprite
    {
        public int X;
        public int Y;
        public int Type;
        public int Height;
        public int Width;
        public int ID;
        public Rectangle rDoors;
        public bool DestroyMe = false;
        public void Draw()
        {            
             rDoors = new Rectangle(X, Y, 100, 150);
             SpriteManager.spriteBatch.Draw(GameStatic.tx2Doors, rDoors, Color.White);         
        }
        public Rectangle Bounds()
        {
            return new Rectangle(X, Y, Width, Height);
        }
        public void Update()
        {

        }
    }
}
