﻿using HologramSpriteManager;
using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StellarRats
{
    class MapTransporter 
    {
        public int X;
        public int Y;
        public int Height;
        public int Width;
        public Rectangle rTeleport;
        public void Draw()
        {
            rTeleport = new Rectangle(X, Y, 50, 50);
            SpriteManager.spriteBatch.Draw(GameStatic.tx2Transporter, rTeleport, Color.White);
        }

        public Rectangle Bounds()
        {
            return new Rectangle(X, Y, Width, Height);
        }
        
    }
}
