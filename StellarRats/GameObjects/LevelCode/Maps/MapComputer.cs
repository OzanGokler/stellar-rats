﻿using HologramSpriteManager;
using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StellarRats
{
    class MapComputer
    {
        public int X;
        public int Y;
        public int Height;
        public int Width;
        public int ID;
        public Rectangle rComputer;

        public bool enemOnComp=false;
        public bool enemToComp = false;

        public void Draw()
        {        
            rComputer = new Rectangle(X, Y, 50, 50);
            SpriteManager.spriteBatch.Draw(GameStatic.tx2Computer, rComputer, Color.White);      
        }
        public Rectangle Bounds()
        {
            return new Rectangle(X, Y, Width, Height);
        }
    }
}
