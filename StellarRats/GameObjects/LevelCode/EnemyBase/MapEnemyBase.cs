﻿using HologramSpriteManager;
using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StellarRats
{
    public class MapEnemyBase
    {

        //Meta Data
        public int X;
        public int Y;
        public int Type;
        public int MaxEnemy;
        public int TimeInterval;
        public int HitPoint;
        public int halfDamage;

        //AI's components
        public bool isAlive = true;
        public bool sendGuard;
        public static bool compIsOk;
        public int ID;
        public Vector2 sendTargetPosition;

        //Dynamic Data
        public int totalHitPoint;
        public double totalTimeInterval;
        public double effectingRate;
        public int currentMaxEnemy = 0;
        public int enemyEffectingRate;
        float fLastSpawnTime = 0;
        public int NumberOfLiveEnemies = 0;

        public MapEnemyBase()
        {
            TimeInterval = GameDatabase._EnemyBaseTypes.BaseTypes[Type].EnemyTime;
            MaxEnemy = GameDatabase._EnemyBaseTypes.BaseTypes[Type].MaxOutEnemy;
            HitPoint = GameDatabase._EnemyBaseTypes.BaseTypes[Type].HitPoint;
            halfDamage = GameDatabase._EnemyBaseTypes.BaseTypes[Type].HalfDamage;

            totalHitPoint = HitPoint;
            totalTimeInterval = (double)TimeInterval;
            currentMaxEnemy = MaxEnemy;
        }

        public void Update()
        {
            if (totalHitPoint > 0)
            {
                float fCurrentTime = SpriteManager.GameTime;
                float fTimePassed = fCurrentTime - fLastSpawnTime;

                if (currentMaxEnemy > NumberOfLiveEnemies && fTimePassed > totalTimeInterval)
                {
                    //Create Enemy.
                    CreateEnemy();
                    fLastSpawnTime = fCurrentTime;


                    NumberOfLiveEnemies++;
                }
            }
            else return;
        }


        public void Draw()
        {
            if (totalHitPoint > 0)
            {
                if (totalHitPoint > halfDamage)
                {
                    Rectangle rEnemyBase = new Rectangle(X, Y, 50, 50);
                    SpriteManager.spriteBatch.Draw(GameStatic.tx2EnemyBase[Type], rEnemyBase, Color.White);
                }
                else if (totalHitPoint <= halfDamage)
                {
                    Rectangle rEnemyBase = new Rectangle(X, Y, 50, 50);
                    SpriteManager.spriteBatch.Draw(GameStatic.tx2HalfDamagedEnemyBase[Type], rEnemyBase, Color.White);

                }
            }

            else
            {
                Rectangle rEnemyBase = new Rectangle(X, Y, 50, 50);
                SpriteManager.spriteBatch.Draw(GameStatic.tx2DamagedEnemyBase[Type], rEnemyBase, Color.White);
            }
            //if (HitPoint > 0)
            //{


            //    if ((float)(totalHitPoint / HitPoint) < 2.5f)
            //    { Rectangle rEnemyBase = new Rectangle(X, Y, 50, 50);
            //        SpriteManager.spriteBatch.Draw(GameStatic.tx2EnemyBase[Type], rEnemyBase, Color.White);


            //    }
            //    else if ((float)(totalHitPoint / HitPoint)< 6 && totalHitPoint /HitPoint >= 2.5f)
            //    {
            //        Rectangle rEnemyBase = new Rectangle(X, Y, 50, 50);
            //        SpriteManager.spriteBatch.Draw(GameStatic.tx2HalfDamagedEnemyBase[Type], rEnemyBase, Color.White);
            //    }
            //    else
            //    {
            //        Rectangle rEnemyBase = new Rectangle(X, Y, 50, 50);
            //        SpriteManager.spriteBatch.Draw(GameStatic.tx2DamagedEnemyBase[Type], rEnemyBase, Color.White);

            //    }
            //}
            //else
            //{
            //    HitPoint = 1;
            //    //Rectangle rEnemyBase = new Rectangle(X, Y, 50, 50);
            //    //SpriteManager.spriteBatch.Draw(GameStatic.tx2DamagedEnemyBase[Type], rEnemyBase, Color.White);
            //}

        }

        public void CreateEnemy()
        {
            //Yukarda Type vs. hepsi zaten oluşturuluyor.
            //Type ı eğer0 sa Preventer;
            //type ı 1 se Crawler ... 
            //type ı 2 se gibi gibi ...
            switch (Type)
            {
                case 0:
                    CreatePreventer(Type);
                    break;

                case 1:
                    break;
                case 2:
                    break;
                case 3:
                    break;

                default:

                    Console.WriteLine("Enemy secim case i calismadi!!");
                    break;
            }

        }
        public void Hit(int iDamage)
        {
            totalHitPoint -= iDamage;
            if (totalHitPoint > 0)
            {
                effectingRate = Math.Cos((totalHitPoint * Math.PI) / (2 * HitPoint));
                totalTimeInterval = TimeInterval + effectingRate * (double)TimeInterval;
                enemyEffectingRate = (int)(effectingRate * 10);

                if (MaxEnemy > enemyEffectingRate)
                    currentMaxEnemy = MaxEnemy - enemyEffectingRate;
            }
            else
            {
                isAlive = false;
         
            }

        }
        private void CreatePreventer(int Type)
        {
            Enemy preventer = new PreventerEnemy(GameStatic.TheEnemyObjects[Type].Clone());            
            preventer.iBaseID = ID;
            preventer.Position = new Vector2(X, Y);
            preventer.EnemID += 1;
            preventer.targetPos = sendTargetPosition;

         
            if (sendGuard)
            {
                preventer._enemyTask = Enemy.EnemyTask.Guard; 
              // görev vericek
                //diğer yaratılanlar da hemen guard atanmasın taskı olan gitsin sadece.
            }
            else
                preventer._enemyTask = Enemy.EnemyTask.Militant;

            GameStatic.lstEnemy.Add(preventer);
        }
    }
}

