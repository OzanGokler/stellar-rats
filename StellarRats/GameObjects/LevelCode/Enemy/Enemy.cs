﻿using HologramSpriteManager;
using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StellarRats
{
    public class Enemy : AnimatedSprite
    { //meta data
        public int health;
        public int power;
        public float speed;

        public Vector2 targetPos;
        public enum EnemyTask
        {  Militant,
            Guard
          
        };
        Vector2 newPos;

        public int iBaseID ;
        public int EnemID ;

        public bool DestroyMe = false;

        public Character.DirectionState Direction = Character.DirectionState.Right;
        public EnemyTask _enemyTask;

        public Enemy(AnimatedSpriteSequences Meta)
            : base(Meta)
        {

        }
        public Enemy()
        { }

        public Enemy(AnimatedSprite Clone)
        {
            Sequences = Clone.Sequences;

        }


        public void Hit(int iDamage)
        {
            health -= iDamage;

            if (health <= 0)
            {
                ///!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!--BasedID ye bak!! kontrol etmen gerek.
                GameStatic.CurrentLevel.EnemyBases[iBaseID].NumberOfLiveEnemies--;
                DestroyMe = true;

            }
            

        }

        public virtual void Update()
        {

        }

       

        public bool Movement(float targetX, float targetY)
        {
            newPos = new Vector2(Position.X, Position.Y);
            if (targetX > Position.X)
            {
                newPos.X += speed;
                Console.WriteLine("newPos > X:" + newPos.X);
            }
            else if (targetX < Position.X)
            {
                newPos.X -= speed;
                Console.WriteLine("newPos < X:" + newPos.X);
            }

            if (targetY > Position.Y)
            {
                newPos.Y += speed;
                Console.WriteLine("newPos >Y:" + newPos.Y);

            }
            else if (targetY < Position.Y)
            {
                newPos.Y -= speed;
                Console.WriteLine("newPos < Y:" + newPos.Y);
            }

            //check if we collide
            Character.DirectionState PossibleDirection;
            if (newPos.X > Position.X)
            {
                if (newPos.Y > Position.Y)
                    PossibleDirection = Character.DirectionState.DownRight;
                else if (newPos.Y < Position.Y)
                    PossibleDirection = Character.DirectionState.UpRight;
                else
                    PossibleDirection = Character.DirectionState.Right;
            }
            else if (newPos.X < Position.X)
            {
                if (newPos.Y > Position.Y)
                    PossibleDirection = Character.DirectionState.DownLeft;
                else if (newPos.Y < Position.Y)
                    PossibleDirection = Character.DirectionState.UpLeft;
                else
                    PossibleDirection = Character.DirectionState.Left;
            }
            else
            {
                if (newPos.Y > Position.Y)
                    PossibleDirection = Character.DirectionState.Down;
                else if (newPos.Y < Position.Y)
                    PossibleDirection = Character.DirectionState.Up;
                else
                    return false;
            }

            bool canMove = CanMove(PossibleDirection);
            Console.WriteLine(canMove);

            if (canMove)
            {

                Position.X = newPos.X;
                Position.Y = newPos.Y;
                return false;
            }
            else
            {

                return false;
            }


        }
        public bool CanMove(Character.DirectionState ProposedDirection)
        {
            for (int i = 0; i < GameStatic.CurrentLevel.WallColliders.Count; i++)
            {
                WallCollider CurrentCollider = GameStatic.CurrentLevel.WallColliders[i];

                if (ProposedDirection == Character.DirectionState.Down || ProposedDirection == Character.DirectionState.DownLeft || ProposedDirection == Character.DirectionState.DownRight)
                {
                    Collision currentCollision = CheckPixelCollision(CurrentCollider.rTop);
                    if (currentCollision.bCollided)
                    {
                        return false;
                    }
                }

                if (ProposedDirection == Character.DirectionState.Up || ProposedDirection == Character.DirectionState.UpLeft || ProposedDirection == Character.DirectionState.UpRight)
                {

                    Collision currentCollision = CheckPixelCollision(CurrentCollider.rBottom);
                    if (currentCollision.bCollided)
                    {
                        return false;
                    }
                }

                if (ProposedDirection == Character.DirectionState.Right || ProposedDirection == Character.DirectionState.UpRight || ProposedDirection == Character.DirectionState.DownRight)
                {

                    Collision currentCollision = CheckPixelCollision(CurrentCollider.rLeft);
                    if (currentCollision.bCollided)
                    {
                        return false;
                    }
                }
                if (ProposedDirection == Character.DirectionState.Left || ProposedDirection == Character.DirectionState.UpLeft || ProposedDirection == Character.DirectionState.DownLeft)
                {

                    Collision currentCollision = CheckPixelCollision(CurrentCollider.rRight);
                    if (currentCollision.bCollided)
                    {
                        return false;
                    }
                }
            }
            return true;
        }
    }
}