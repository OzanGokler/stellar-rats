﻿using HologramSpriteManager;
using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StellarRats
{
    public class PreventerEnemy : Enemy
    {
        //controllers
        public bool isGuard = false;


        public Enemy.EnemyTask _preEnTask;
        public PreventerEnemy()
        {
      
        }

        public PreventerEnemy(AnimatedSpriteSequences Meta)
            : base(Meta)
        {
        }

        public PreventerEnemy(AnimatedSprite clone)
            : base(clone)
        {
            Sequences = clone.Sequences;
            //type 0 => preventer
            this.power = GameDatabase._EnemyTypes.enemyTypes[0].power;
            this.speed = GameDatabase._EnemyTypes.enemyTypes[0].speed;
            this.health = GameDatabase._EnemyTypes.enemyTypes[0].health;
        }

        public override void Update()
        {
            CheckPoints(base.targetPos);

        }

        private void CheckPoints(Vector2 targetPosition)
        {
            _preEnTask = base._enemyTask;
            if (_preEnTask == EnemyTask.Guard )
            {
                base.Movement(targetPosition.X, targetPosition.Y);
               
               //******************
                //if (Position.X == targetPosition.X && Position.Y == targetPosition.Y)
                //{
                //    GameStatic.CurrentLevel.Computer[0].enemOnComp = true;
                //}
            }
            if (_preEnTask == EnemyTask.Militant)
            {
                Position.X -= 1;
            }
        }

    }

}


