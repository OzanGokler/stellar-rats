﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StellarRats
{
    class Level
    {
        public string sDescription;

        public List<MapGrounds> Grounds;
        public List<MapWalls> Walls;
        public List<MapEnemyBase> EnemyBases;
        public List<WallCollider> WallColliders;
        public List<DoorCollider> DoorColliders;
        public List<MapTransporter> Transporters;
        public List<MapHealth> Healths;
        public List<MapComputer> Computer;
        public List<MapDoors> Doors;

        public void Initialise()
        {
            //set base ids
            for (int i = 0; i < GameStatic.CurrentLevel.EnemyBases.Count; i++)
            {
                GameStatic.CurrentLevel.EnemyBases[i].ID = i;
            }
            //set up wall colliders
            SetUpWallColliders();
            SetUpDoorColliders();
        }
        public static void SetUpWallColliders()
        {
            for (int i = 0; i < GameStatic.CurrentLevel.WallColliders.Count; i++)
            {
                GameStatic.CurrentLevel.WallColliders[i].SetRectangles();
            }         
        }
        public static void SetUpDoorColliders()
        {
            for (int i = 0; i < GameStatic.CurrentLevel.DoorColliders.Count; i++)
            {
                GameStatic.CurrentLevel.DoorColliders[i].SetRectangles();
            }
        }

        public static void Update()
        {
            GameStatic.CurrentLevel.DoorColliders.RemoveAll(item => item.bDestroyMe == true);
        }
    }
}
