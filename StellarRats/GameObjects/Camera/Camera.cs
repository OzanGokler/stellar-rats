using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using StellarRats;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StellarRats
{
    class Camera
    {
        int HalfX = 960;
        int HalfY = 540;

        float commoncenterX;
        float commoncenterY;
        public Matrix transform;
        public bool bcamCheck = false;
        Viewport view;
        public Vector2 TopLeft;

        public Camera(Viewport newView)
        {
            view = newView;
            //centre = new Vector2( -960,  -540);
        }

        enum ChangeValue
        { 
            Reduce,
            Increase,
            Leave
        }

        float CameraSpeed = 3f;
        float CenterBumper = 3f;
        public void Update(GameTime gameTime)
        {

            List<Player> ActivePlayers = GameStatic.ThePlayers.Where(item => item.IsActive == true).ToList();

            float iTotalX = ActivePlayers.Sum(x => x.Position.X);
            float iTotalY = ActivePlayers.Sum(x => x.Position.Y);


            commoncenterX = iTotalX / ActivePlayers.Count;
            commoncenterY = iTotalY / ActivePlayers.Count;

            Vector2 TargetTopLeft = new Vector2(commoncenterX - HalfX, commoncenterY - HalfY);

            //find camera speed
            float fDistance = Math.Abs( Vector2.Distance(TargetTopLeft, TopLeft));
            if (fDistance < 100)
            {
                CameraSpeed = 1;
            }
            else
            {
                CameraSpeed = fDistance / 60;
            }


            float NewY = TopLeft.Y;
            float NewX = TopLeft.X;
            ChangeValue ChangeX = ChangeValue.Leave;
            ChangeValue ChangeY = ChangeValue.Leave;


            if (TopLeft.X + CenterBumper < TargetTopLeft.X)
            {
                ChangeX = ChangeValue.Increase;
                //NewX = TopLeft.X + CameraSpeed;
            }
            else if (TopLeft.X - CenterBumper > TargetTopLeft.X)
            {
                ChangeX = ChangeValue.Reduce;

                //NewX = TopLeft.X - CameraSpeed;
            }
            else
            {
                //NewX = TopLeft.X;
            }


            if (TopLeft.Y + CenterBumper < TargetTopLeft.Y)
            {
                ChangeY = ChangeValue.Increase;
                //NewY = TopLeft.Y + CameraSpeed;
            }
            else if (TopLeft.Y - CenterBumper > TargetTopLeft.Y)
            {
                ChangeY = ChangeValue.Reduce;
                //NewY = TopLeft.Y - CameraSpeed;
            }
            else
            {
                //NewY = TopLeft.Y;
            }



            if (ChangeX == ChangeValue.Increase && ChangeY == ChangeValue.Leave)
            {
                NewX = TopLeft.X + CameraSpeed; 
            }
            else if (ChangeX == ChangeValue.Reduce && ChangeY == ChangeValue.Leave)
            {
                NewX = TopLeft.X - CameraSpeed; 
            }
            else if (ChangeY == ChangeValue.Increase && ChangeX == ChangeValue.Leave)
            {
                NewY = TopLeft.Y + CameraSpeed; 
            }
            else if (ChangeY == ChangeValue.Reduce && ChangeX == ChangeValue.Leave)
            {
                NewY = TopLeft.Y - CameraSpeed; 
            }
            else if (ChangeX == ChangeValue.Increase && ChangeY == ChangeValue.Increase)
            {
                Vector2 NewDistance = DiagonalDistance(TargetTopLeft);
                /*
                float DistanceX = Math.Abs( TargetTopLeft.X - TopLeft.X);
                float DistanceY = Math.Abs(TargetTopLeft.Y - TopLeft.Y);

                float ratio = DistanceY / DistanceX ;

                double NewXDistance = Math.Sqrt( (double)  ((CameraSpeed*CameraSpeed) /(1 + ratio))  );
                double NewYDistance = ratio * NewXDistance;

                double hyp = Math.Sqrt((NewXDistance * NewXDistance) + (NewYDistance * NewYDistance));
                */
                NewX = TopLeft.X + NewDistance.X;
                NewY = TopLeft.Y + NewDistance.Y; 

            }
            else if (ChangeX == ChangeValue.Reduce && ChangeY == ChangeValue.Reduce)
            {
                Vector2 NewDistance = DiagonalDistance(TargetTopLeft);
                NewX = TopLeft.X - NewDistance.X;
                NewY = TopLeft.Y - NewDistance.Y; 
            }
            else if (ChangeX == ChangeValue.Increase && ChangeY == ChangeValue.Reduce)
            {
                Vector2 NewDistance = DiagonalDistance(TargetTopLeft);
                NewX = TopLeft.X + NewDistance.X;
                NewY = TopLeft.Y - NewDistance.Y;

            }
            else if (ChangeX == ChangeValue.Reduce && ChangeY == ChangeValue.Increase)
            {
                Vector2 NewDistance = DiagonalDistance(TargetTopLeft);

                NewX = TopLeft.X - NewDistance.X;
                NewY = TopLeft.Y + NewDistance.Y;
            }

            TopLeft = new Vector2(NewX, NewY);
            Vector2 _TopLeft = new Vector2(NewX /*+ (HalfX*2)*/, NewY /*+ (2*HalfY)*/);
            transform = Matrix.CreateScale(new Vector3(1, 1, 0)) * Matrix.CreateTranslation(new Vector3(-_TopLeft.X, -_TopLeft.Y, 0));
        }

        Vector2 DiagonalDistance(Vector2 TargetTopLeft)
        {
            Vector2 ret;
            float DistanceX = Math.Abs((TargetTopLeft.X - TopLeft.X));
            float DistanceY = Math.Abs((TargetTopLeft.Y - TopLeft.Y));

            float ratio = Math.Abs(DistanceY / DistanceX);

            double NewXDistance = Math.Sqrt((double)((CameraSpeed * CameraSpeed) / (1 + ratio)));
            double NewYDistance = ratio * NewXDistance;
            
            double hyp = Math.Sqrt((NewXDistance * NewXDistance) + (NewYDistance * NewYDistance));

            ret = new Vector2( (float)NewXDistance, (float)NewYDistance );
            return ret;
        }
    }
}
