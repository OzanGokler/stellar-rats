﻿using HologramSpriteManager;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StellarRats
{
    class GameDatabase
    {
        public static EnemyBaseTypes _EnemyBaseTypes;
        public static EnemyTypes _EnemyTypes;
        public static Enemy _Enemy;

        public static GunTypes _GunTypes;
        public static Missile _Missiles;
        public static Node _Nodes;
        public GameDatabase()

        {   
            //Nodes
            string nodeTextFile = System.IO.File.ReadAllText(@".\Content\Nodes\Node1.json");
           _Nodes = JsonConvert.DeserializeObject<Node>(nodeTextFile);
            
            //Enemy Types
            string etTextFile = System.IO.File.ReadAllText(@".\Content\Enemy\EnemyTypes.json");
            _EnemyTypes = JsonConvert.DeserializeObject<EnemyTypes>(etTextFile);
            
            //Enemy Base Types
            string ebtTextFile = System.IO.File.ReadAllText(@".\Content\EnemyBase\EnemyBaseTypes.json");
            _EnemyBaseTypes = JsonConvert.DeserializeObject<EnemyBaseTypes>(ebtTextFile);

            //Enemy
            for (int i = 0; i <_EnemyTypes.enemyTypes.Count; i++)
            {
                _Enemy = AnimationSpecReader.PopulateAnimations<Enemy>(@".\Content\Enemy\" + _EnemyTypes.enemyTypes[i].animationJson);
                GameStatic.TheEnemyObjects.Add(_Enemy);
            }
           

            //GunTypes
            string gunText = System.IO.File.ReadAllText(@".\Content\Specs\Guns.json");
           _GunTypes = JsonConvert.DeserializeObject<GunTypes>(gunText);

            //Missile Types
           _Missiles = AnimationSpecReader.PopulateAnimations<Missile>(@".\Content\Specs\Missile.json");
        }
    }
}
